<?php 
/**
* Plugin Name: userProfile
* Description: Plugin to add functionality to user profile by adding the ability to add extra details
* Author: Kenneth Kisakye
* Version: 1.0
*/


add_action( 'show_user_profile', 'extra_user_profile_fields' );
add_action( 'edit_user_profile', 'extra_user_profile_fields' );

function extra_user_profile_fields( $user ) { ?>
    <h3><?php _e("Extra User information", "blank"); ?></h3>

    <table class="form-table">
    <tr>
        <th><label for="user_age"><?php _e("User's Age"); ?></label></th>
        <td>
            <input type="number" name="user_age" id="user_age" value="<?php echo esc_attr( get_the_author_meta( 'user_age', $user->ID ) ); ?>" class="regular-text" /><br />
            <span class="description"><?php _e("Please enter your User's age."); ?></span>
        </td>
    </tr>
    <tr>
        <th><label for="sex"><?php _e("Sex"); ?></label></th>
        <td>
            <input type="text" name="sex" id="sex" value="<?php echo esc_attr( get_the_author_meta( 'sex', $user->ID ) ); ?>" class="regular-text" /><br />
            <span class="description"><?php _e("Please enter user's sex."); ?></span>
        </td>
    </tr>
    <tr>
    <th><label for="programming_language"><?php _e("Preferred programming language"); ?></label></th>
        <td>
            <input type="text" name="programming_language" id="programming_language" value="<?php echo esc_attr( get_the_author_meta( 'programming_language', $user->ID ) ); ?>" class="regular-text" /><br />
            <span class="description"><?php _e("Please enter your Preferred programming language."); ?></span>
        </td>
    </tr>

     <tr>
    <th><label for="preferred_cms"><?php _e("Preferred content management Systen"); ?></label></th>
        <td>
            <input type="text" name="preferred_cms" id="preferred_cms" value="<?php echo esc_attr( get_the_author_meta( 'preferred_cms', $user->ID ) ); ?>" class="regular-text" /><br />
            <span class="description"><?php _e("Please enter your Preferred CMS."); ?></span>
        </td>
    </tr>
    </table>
<?php }
	
?>